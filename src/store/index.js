import { createStore } from 'vuex'

export default createStore({
    state: () => ({
        fi: [
            {
                name: 'Dir 1',
                list: [
                    {
                        name: 'Dir 1-1',
                        list: [
                            {
                                name: 'File 1-1-1',
                            },
                        ],
                    },
                    {
                        name: 'File 1-2',
                    },
                ],
            },
            {
                name: 'Dir 2',
                list: [
                    {
                        name: 'Dir 2-1',
                        list: []
                    },
                    {
                        name: 'File 2-2',
                    },
                ],
            },
            {
                name: 'File 1',
            },
        ],
    }),
    getters: {
        fileItems(state) {
            return state.fi
        }
    },
    mutations: {
        setFI(state, fi) {
            state.fi = fi
        }
    },
    actions: {
        setFI(state, commit) {
            commit('setFI', commit)
        }
    },
})
